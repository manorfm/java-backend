package br.com.user.rest.domain.authentication.service;

import br.com.user.domain.user.User;
import br.com.user.domain.user.service.UserService;
import br.com.user.security.KeySecurity;
import br.com.user.security.KeyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

@Service
public class UserAuthenticationManager implements AuthenticationManager {

    @Autowired
    private UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            String encryptedKey = KeyUtil.doHash(String.valueOf(authentication.getCredentials()), KeySecurity.KEY);
            String email = String.class.cast(authentication.getPrincipal());

            User user = userService.get(email);

            if (!encryptedKey.equals(user.getPassword())) {
                throw new BadCredentialsException("Usu�rio e/ou senha inv�lidos");
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return authentication;
    }
}
