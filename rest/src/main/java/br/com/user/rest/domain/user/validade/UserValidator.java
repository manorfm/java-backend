package br.com.user.rest.domain.user.validade;

import br.com.user.rest.domain.user.exception.InvalidUserCredentialException;
import br.com.user.rest.domain.user.exception.UserCredentialNullException;
import br.com.user.rest.domain.user.exception.UserResourceEmptyException;
import br.com.user.rest.domain.user.resources.UserResource;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidator {

    public static void validate(UserResource user) {
        if (isEmpty(user)) {
            throw new UserResourceEmptyException();
        }
        if (isEmpty(user.getEmail()) && isEmpty(user.getPassword()) && isEmpty(user.getName())) {
            throw new UserResourceEmptyException();
        }
        if (isEmpty(user.getEmail()) || isEmpty(user.getPassword())) {
            throw new UserCredentialNullException();
        }

        validateEmail(user.getEmail());
    }

    private static void validateEmail(String email) {
        Pattern pattern = Pattern.compile("^.+@.+\\..+$");
        Matcher matcher = pattern.matcher(email);

        if (!matcher.matches()) {
            throw new InvalidUserCredentialException();
        }
    }

    private static boolean isEmpty(Object o) {
        if (o == null) {
            return true;
        }

        if (o instanceof String && "".equals(String.class.cast(o))) {
            return true;
        }

        return false;
    }
}
