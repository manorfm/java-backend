package br.com.user.rest.domain.authentication.handler;

import br.com.user.exception.ErrorMessage;
import br.com.user.exception.UserNotFoundException;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity handleUserNotFound(HttpServletRequest request, Exception e) {
        ErrorMessage errorMessage = new ErrorMessage("error", e.getMessage());

        return new ResponseEntity(errorMessage, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity handleBadCredentials(HttpServletRequest request, Exception e) {
        ErrorMessage errorMessage = new ErrorMessage("error", "Usu�rio e/ou senha inv�lidos");

        return new ResponseEntity(errorMessage, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ExpiredJwtException.class)
    public ResponseEntity handleExpired(HttpServletRequest request, Exception e) {
        ErrorMessage errorMessage = new ErrorMessage("error", "Sess�o inv�lida");

        return new ResponseEntity(errorMessage, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(Exception.class)
    @Order(1000)
    public ResponseEntity handleException(HttpServletRequest request, Exception e) {
        ErrorMessage errorMessage = new ErrorMessage("error", "Ocorreu um erro no sistema");

        return new ResponseEntity(errorMessage, HttpStatus.CONFLICT);
    }
}
