package br.com.user.rest.domain.user.exception;

public class InvalidUserCredentialException extends RuntimeException {

    public InvalidUserCredentialException() {
        super("Email is not valid");
    }
}
