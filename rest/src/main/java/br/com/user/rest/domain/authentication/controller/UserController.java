package br.com.user.rest.domain.authentication.controller;

import java.util.List;

import br.com.user.rest.domain.user.assembler.UserResourcesAssembler;
import br.com.user.rest.domain.user.exception.InvalidUserCredentialException;
import br.com.user.rest.domain.user.exception.UserCredentialNullException;
import br.com.user.rest.domain.user.exception.UserResourceEmptyException;
import br.com.user.rest.domain.user.resources.UserResource;
import br.com.user.rest.domain.user.validade.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import br.com.user.domain.user.User;
import br.com.user.domain.user.service.UserService;
import br.com.user.exception.UserAlreadyExistException;
import br.com.user.exception.UserNotFoundException;
import br.com.user.rest.services.AbstractController;

@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class UserController extends AbstractController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserResourcesAssembler userAssembler;
	
	@PostMapping
	public ResponseEntity<?> add(@RequestBody UserResource resource) {
		try {
			UserValidator.validate(resource);

			User user = userAssembler.convert(resource);
			User userSaved = userService.save(user);

			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

			return responseCreated(userAssembler.convert(userSaved));
		} catch (UserAlreadyExistException e) {
			return exception("Create user error!", e);
		} catch(UserCredentialNullException | InvalidUserCredentialException | UserResourceEmptyException e) {
			return exception("Error creating user", e);
		}
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<?> get(@PathVariable String id) {
		try {
			User user = userService.getById(id);
			UserResource resource = userAssembler.convert(user);
		
			return response(resource, HttpStatus.OK);
		} catch (UserNotFoundException e) {
			return exception("User not exist", e);
		}
	}

	@GetMapping
	public ResponseEntity<?> geAll() {
		List<User> users = userService.findAll();

		List<UserResource> resources = userAssembler.convert(users);
		
		return response(resources, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable String id) {
		try {
			userService.remove(id);
			return responseOk();
		} catch (Exception e) {
			return exception("Erro removing", e);
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody UserResource resource, @PathVariable String id) {
		try {
			User user = userService.getById(id);
		
			user.setName(resource.getName());
			user.setPassword(resource.getPassword());
			user.setEmail(resource.getEmail());
			User userUpdated = userService.update(user);

			UserResource userDto = userAssembler.convert(userUpdated);
			return response(userDto, HttpStatus.OK);
		} catch (UserNotFoundException e) {
			return exception("Erro updating.", e);
		}
	}
}