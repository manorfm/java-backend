package br.com.user.rest.domain.authentication.controller;

import br.com.user.domain.user.Phone;
import br.com.user.domain.user.User;
import br.com.user.domain.user.service.UserService;
import br.com.user.exception.UserAlreadyExistException;
import br.com.user.exception.UserNotFoundException;
import br.com.user.rest.domain.user.assembler.PhoneResourcesAssembler;
import br.com.user.rest.domain.user.assembler.UserResourcesAssembler;
import br.com.user.rest.domain.user.resources.PhoneResource;
import br.com.user.rest.domain.user.resources.UserResource;
import br.com.user.rest.services.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class PhoneController extends AbstractController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private PhoneResourcesAssembler phoneAssembler;

	@Autowired
	private UserResourcesAssembler userAssembler;
	
	@PostMapping("/{id}/phones")
	public ResponseEntity<?> add(@PathVariable String id, @RequestBody PhoneResource resource) {
		try {
			Phone phone = phoneAssembler.convert(resource);
			User user = userService.getById(id);
			user.add(phone);
			User userUpdated = userService.update(user);
			return responseCreated(userAssembler.convert(userUpdated));
		} catch (UserAlreadyExistException e) {
			return exception("Create user error!", e);
		}
	}

	@DeleteMapping("/{id}/phones/{idPhone}")
	public ResponseEntity<?> delete(@PathVariable String id, @PathVariable String idPhone) {
		try {
			userService.removePhone(id, idPhone);
			return responseOk();
		} catch (Exception e) {
			return exception("Error while removing phone number", e);
		}
	}
	
	@PutMapping("/{id}/phones/{idPhone}")
	public ResponseEntity<?> update(@PathVariable String id, @PathVariable String idPhone, @RequestBody PhoneResource resource) {
		try {
			Phone phone = phoneAssembler.convert(resource);

			User user = userService.getById(id);
			User userUpdated = userService.update(user, idPhone, phone);

			UserResource userDto = userAssembler.convert(userUpdated);
			return response(userDto, HttpStatus.OK);
		} catch (UserNotFoundException e) {
			return exception("Error while updating phone number.", e);
		}
	}
}