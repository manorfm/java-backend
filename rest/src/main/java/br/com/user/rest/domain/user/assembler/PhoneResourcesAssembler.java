package br.com.user.rest.domain.user.assembler;

import br.com.user.domain.user.Phone;
import br.com.user.rest.domain.user.resources.PhoneResource;
import br.com.user.rest.services.AbstractAssembler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PhoneResourcesAssembler extends AbstractAssembler<Phone, PhoneResource> {
   
	public List<PhoneResource> convert(List<Phone> phones) {
    	return phones.stream()
			.map(user -> mapper.map(user, PhoneResource.class))
			.collect(Collectors.toList());
    }
    
    public PhoneResource convert(Phone phone) {
        return mapper.map(phone, PhoneResource.class);
    }
    
    public Phone convert(PhoneResource resource) {
    	return mapper.map(resource, Phone.class);
    }
    
}
