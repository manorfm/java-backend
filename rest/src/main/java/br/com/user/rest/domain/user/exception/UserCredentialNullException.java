package br.com.user.rest.domain.user.exception;

public class UserCredentialNullException extends RuntimeException {

    public UserCredentialNullException() {
        super("User email and password need to be informed");
    }
}
