package br.com.user.rest.domain.user.exception;

public class UserResourceEmptyException extends RuntimeException {

    public UserResourceEmptyException() {
        super("User data have to be informed");
    }
}
