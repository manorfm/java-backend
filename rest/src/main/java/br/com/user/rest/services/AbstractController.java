package br.com.user.rest.services;

import br.com.user.exception.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class AbstractController {

    public ResponseEntity<String> responseOk() {
        return response(HttpStatus.OK);
    }
    
    public <T> ResponseEntity<T> responseCreated(T t) {
    	return response(t, HttpStatus.CREATED);
    }
    
    public <T> ResponseEntity<T> responseOk(T t) {
        return response(t, HttpStatus.OK);
    }
    
    public <T> ResponseEntity<T> responseError(HttpStatus status) {
    	return response(status);
    }

    public <T> ResponseEntity<T> responseError(T t,HttpStatus status) {
    	return response(t, status);
    }
    
    private <T> ResponseEntity<T> response(HttpStatus status) {
    	return new ResponseEntity<T>(status);
    }
    
    public <T> ResponseEntity<T> response(T t, HttpStatus status) {
    	return new ResponseEntity<T>(t, status);
    }
    
    protected String toString(Object o) {
    	if (o == null) {
    		return null;
    	}
    	
    	return String.valueOf(o);
    }
    
    protected ResponseEntity<?> exception(String title, Exception e) {
        ErrorMessage message = new ErrorMessage(title, e.getMessage());
		return responseError(message, HttpStatus.CONFLICT);
	}
}
