package br.com.user.rest.domain.authentication.controller;

import br.com.user.domain.user.User;
import br.com.user.domain.user.service.UserService;
import br.com.user.exception.BusinessException;
import br.com.user.exception.ErrorMessage;
import br.com.user.rest.domain.authentication.model.JwtRequest;
import br.com.user.rest.domain.authentication.model.JwtResponse;
import br.com.user.rest.domain.authentication.service.JwtUserDetailsService;
import br.com.user.rest.domain.authentication.service.UserAuthenticationManager;
import br.com.user.rest.domain.authentication.util.JwtTokenUtil;
import br.com.user.rest.domain.user.assembler.UserResourcesAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class JwtAuthenticationController {

    @Autowired
    private UserAuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserResourcesAssembler userAssembler;

    @PostMapping(value = "/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest request) throws Exception {
        authenticate(request.getEmail(), request.getPassword());
        final String token = jwtTokenUtil.generateToken(request.getEmail());
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @PostMapping(value = "/signin")
    public ResponseEntity<?> signin(@RequestBody JwtRequest request) throws Exception {

        User user = new User();
        user.setEmail(request.getEmail());
        user.setPassword(request.getPassword());

        userService.save(user);

        return ResponseEntity.of(Optional.empty());
    }

    private Authentication authenticate(String email, String password) throws Exception {
        try {
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException("INVALID_CREDENTIALS", e);
        }
    }
}
