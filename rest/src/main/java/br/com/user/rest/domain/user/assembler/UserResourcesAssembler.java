package br.com.user.rest.domain.user.assembler;

import java.util.List;
import java.util.stream.Collectors;

import br.com.user.rest.domain.user.resources.UserResource;
import org.springframework.stereotype.Component;

import br.com.user.domain.user.User;
import br.com.user.rest.services.AbstractAssembler;

@Component
public class UserResourcesAssembler extends AbstractAssembler<User, UserResource> {
   
	public List<UserResource> convert(List<User> users) {
    	return users.stream()
			.map(user -> mapper.map(user, UserResource.class))
			.collect(Collectors.toList());
    }
    
    public UserResource convert(User user) {
        return mapper.map(user, UserResource.class);
    }
    
    public User convert(UserResource resource) {
    	return mapper.map(resource, User.class);
    }
    
}
