package br.com.user.rest.domain.authentication.model;

import lombok.AllArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
public class JwtResponse implements Serializable {

    private final String token;

    public String getToken() {
        return this.token;
    }
}