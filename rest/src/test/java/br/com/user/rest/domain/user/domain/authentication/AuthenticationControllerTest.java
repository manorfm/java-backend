package br.com.user.rest.domain.user.domain.authentication;

import br.com.user.domain.user.service.UserService;
import br.com.user.rest.domain.authentication.controller.JwtAuthenticationController;
import br.com.user.rest.domain.authentication.model.JwtRequest;
import br.com.user.rest.domain.authentication.service.UserAuthenticationManager;
import br.com.user.rest.domain.authentication.util.JwtTokenUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AuthenticationControllerTest {

    private ObjectMapper mapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Spy
    private JwtTokenUtil jwtTokenUtil;

    @Mock
    private UserAuthenticationManager authenticationManager;

    @Mock
    private UserService userService;

    @InjectMocks
    private JwtAuthenticationController authenticationController;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(authenticationController)
                .build();
    }

    @Test(expected = NestedServletException.class)
    public void givenDoLogin_AndAuthenticatedManager_AndThrowError() throws Exception {
        when(authenticationManager.authenticate(isA(Authentication.class))).thenThrow(new BadCredentialsException(""));

        JwtRequest request = new JwtRequest();
        request.setPassword("1234");
        request.setEmail("teste@teste.com");

        String json = mapper.writeValueAsString(request);
        mockMvc.perform(post("/login").content(json).contentType(MediaType.APPLICATION_JSON));
        verify(authenticationManager, times(1)).authenticate(isA(Authentication.class));
        verify(jwtTokenUtil, times(0)).generateToken(isA(String.class));

        verifyNoMoreInteractions(authenticationManager);
        verifyNoMoreInteractions(jwtTokenUtil);
    }

    @Test
    public void givenDoLogin_AndAuthenticatedManagerPass_ThenShouldReturnAToken() throws Exception {

        JwtRequest request = new JwtRequest();
        request.setPassword("1234");
        request.setEmail("teste@teste.com");

        String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtYW5vcmZtQGhvdG1haWwuY29tIiwiaWF0IjoxNTc5NTQ5NDM5LCJleHAiOjE1N" +
                "zk1NDk0NDF9.O4HtBp8jO3w5hOB7pR9K2nEJ9pgnr5mS-kGnqSq8Yo6BrGuuUpACQoaHnzPaLPivLDFxKv0BSUsEG1P-feP1rg";
        when(authenticationManager.authenticate(isA(Authentication.class))).thenReturn(null);
        when(jwtTokenUtil.generateToken(request.getEmail())).thenReturn(token);

        String json = mapper.writeValueAsString(request);
        mockMvc.perform(post("/login").content(json).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("token", is(token)));
        verify(authenticationManager, times(1)).authenticate(isA(Authentication.class));
        verify(jwtTokenUtil, times(1)).generateToken(isA(String.class));

        verifyNoMoreInteractions(authenticationManager);
        verifyNoMoreInteractions(jwtTokenUtil);
    }
}