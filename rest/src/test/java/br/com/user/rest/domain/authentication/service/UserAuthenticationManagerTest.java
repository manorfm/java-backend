package br.com.user.rest.domain.authentication.service;

import br.com.user.domain.user.User;
import br.com.user.domain.user.service.UserService;
import br.com.user.security.KeySecurity;
import br.com.user.security.KeyUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.*;

public class UserAuthenticationManagerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserAuthenticationManager userAuthenticationManager;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void givenEmailAndPasswordValid_ThenShouldPassWithoutAnyIssues() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String email = "manoel@test.com";
        String password = "1234";

        String encryptedPassword = null;
        encryptedPassword = KeyUtil.doHash(password, KeySecurity.KEY);

        User user = new User();
        user.setEmail(email);
        user.setPassword(encryptedPassword);

        Authentication authentication = mock(Authentication.class);

        when(authentication.getCredentials()).thenReturn(password);
        when(authentication.getPrincipal()).thenReturn(email);
        when(userService.get(email)).thenReturn(user);

        userAuthenticationManager.authenticate(authentication);

        verify(authentication, times(1)).getCredentials();
        verify(authentication, times(1)).getPrincipal();
        verify(userService, times(1)).get(email);

        verifyNoMoreInteractions(authentication);
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void givenInvalidPassword_ThenShouldThrowAError() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String email = "manoel@test.com";
        String password = "1234";

        String encryptedPassword = null;
        encryptedPassword = KeyUtil.doHash(password, KeySecurity.KEY);

        User user = new User();
        user.setEmail(email);
        user.setPassword(encryptedPassword);

        Authentication authentication = mock(Authentication.class);

        when(authentication.getCredentials()).thenReturn(password + "5");
        when(authentication.getPrincipal()).thenReturn(email);
        when(userService.get(email)).thenReturn(user);

        Exception exception = Assertions.assertThrows(BadCredentialsException.class, () -> {
            userAuthenticationManager.authenticate(authentication);
        });

        assertThat(exception.getMessage(), equalTo("Usu�rio e/ou senha inv�lidos"));

        verify(authentication, times(1)).getCredentials();
        verify(authentication, times(1)).getPrincipal();
        verify(userService, times(1)).get(email);

        verifyNoMoreInteractions(authentication);
        verifyNoMoreInteractions(userService);
    }
}