package br.com.user.rest.domain.user.domain.user;

import br.com.user.domain.user.Phone;
import br.com.user.domain.user.User;
import br.com.user.domain.user.service.UserService;
import br.com.user.rest.domain.authentication.controller.PhoneController;
import br.com.user.rest.domain.authentication.controller.UserController;
import br.com.user.rest.domain.user.assembler.PhoneResourcesAssembler;
import br.com.user.rest.domain.user.assembler.UserResourcesAssembler;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;


public class UserControllerTest {

    private ObjectMapper mapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Mock
    private UserService userService;

    @Spy
    private UserResourcesAssembler userAssembler;

    @Spy
    private PhoneResourcesAssembler phoneAssembler;

    @InjectMocks
    private UserController userController;

    @InjectMocks
    private PhoneController phoneController;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(userController, phoneController)
                .build();
    }

    private User getModel() {
        User user = new User();
        user.setName("Manoel Saraiva");
        user.setEmail("massa@test.com");
        user.setPassword("password");

        List<Phone> phones = new ArrayList<>();
        phones.add(new Phone(81, 4983_0912));
        phones.add(new Phone(87, 4983_0863));
        phones.add(new Phone(11, 3123_9387));
        user.setPhones(phones);

        return user;
    }

    private String format(LocalDateTime date) {
        if (date == null) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return date.format(formatter);
    }

    @Test
    public void givenVerbGetInUser_ThenShouldReturnListOfUser() throws Exception {
        User user = getModel();
        List<Phone> phones = user.getPhones();

        List<User> users = Arrays.asList(user);

        when(userService.findAll()).thenReturn(users);
        mockMvc.perform(get("/users").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(user.getName())))
                .andExpect(jsonPath("$[0].email", is(user.getEmail())))
                .andExpect(jsonPath("$[0].password", is(user.getPassword())))
                .andExpect(jsonPath("$[0].created", is(format(user.getCreated()))))
                .andExpect(jsonPath("$[0].modified", is(format(user.getModified()))))
                .andExpect(jsonPath("$[0].lastLogin", is(format(user.getLastLogin()))))
                .andExpect(jsonPath("$[0].phones[0].ddd", is(phones.get(0).getDdd())))
                .andExpect(jsonPath("$[0].phones[0].number", is(phones.get(0).getNumber())))
                .andExpect(jsonPath("$[0].phones[1].ddd", is(phones.get(1).getDdd())))
                .andExpect(jsonPath("$[0].phones[1].number", is(phones.get(1).getNumber())))
                .andExpect(jsonPath("$[0].phones[2].ddd", is(phones.get(2).getDdd())))
                .andExpect(jsonPath("$[0].phones[2].number", is(phones.get(2).getNumber())));


        verify(userService, times(1)).findAll();

        verifyNoMoreInteractions(userService);
    }

    @Test
    public void givenVerbGetInUser_AndId_AndTheUserExist_THenShouldReturnTheUser() throws Exception {
        User user = getModel();
        List<Phone> phones = user.getPhones();

        when(userService.getById("14416af3a404970d0b9ff2a4d5f9c2")).thenReturn(user);
        mockMvc.perform(get("/users/14416af3a404970d0b9ff2a4d5f9c2").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("name", is(user.getName())))
                .andExpect(jsonPath("email", is(user.getEmail())))
                .andExpect(jsonPath("password", is(user.getPassword())))
                .andExpect(jsonPath("created", is(format(user.getCreated()))))
                .andExpect(jsonPath("modified", is(format(user.getModified()))))
                .andExpect(jsonPath("lastLogin", is(format(user.getLastLogin()))))
                .andExpect(jsonPath("phones[0].ddd", is(phones.get(0).getDdd())))
                .andExpect(jsonPath("phones[0].number", is(phones.get(0).getNumber())))
                .andExpect(jsonPath("phones[1].ddd", is(phones.get(1).getDdd())))
                .andExpect(jsonPath("phones[1].number", is(phones.get(1).getNumber())))
                .andExpect(jsonPath("phones[2].ddd", is(phones.get(2).getDdd())))
                .andExpect(jsonPath("phones[2].number", is(phones.get(2).getNumber())));

        verify(userService, times(1)).getById("14416af3a404970d0b9ff2a4d5f9c2");

        verifyNoMoreInteractions(userService);
    }

    @Test
    public void givenVerbPostInUsers_THenShouldSave_AndReturnWithNewData() throws Exception {
        User user = new User();
        user.setName("Manoel Saraiva");
        user.setEmail("massa@test.com");
        user.setPassword("password");

        User userExpected = new User();
        userExpected.setId("14416af3a404970d0b9ff2a4d5f9c2");
        userExpected.setName("Manoel Saraiva");
        userExpected.setEmail("massa@test.com");
        userExpected.setPassword("password");

        when(userService.save(user)).thenReturn(userExpected);

        String json = mapper.writeValueAsString(user);
        mockMvc.perform(post("/users").content(json).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(userExpected.getId())))
                .andExpect(jsonPath("name", is(userExpected.getName())))
                .andExpect(jsonPath("email", is(userExpected.getEmail())))
                .andExpect(jsonPath("password", is(userExpected.getPassword())))
                .andExpect(jsonPath("created", is(format(user.getCreated()))))
                .andExpect(jsonPath("modified", is(format(user.getModified()))))
                .andExpect(jsonPath("lastLogin", is(format(user.getLastLogin()))));

        verify(userService, times(1)).save(user);

        verifyNoMoreInteractions(userService);
    }

    @Test
    public void givenVerbPostInUsers_AndAPhoneList_THenShouldSave_AndReturnWithNewData() throws Exception {
        List<Phone> phones = new ArrayList<>();
        phones.add(new Phone(81, 4983_0912));

        User user = new User();
        user.setName("Manoel Saraiva");
        user.setEmail("massa@test.com");
        user.setPassword("password");
        user.setPhones(phones);

        User userExpected = new User();
        userExpected.setId("14416af3a404970d0b9ff2a4d5f9c2");
        userExpected.setName("Manoel Saraiva");
        userExpected.setEmail("massa@test.com");
        userExpected.setPassword("password");
        userExpected.setPhones(phones);

        when(userService.save(user)).thenReturn(userExpected);

        String json = mapper.writeValueAsString(user);
        mockMvc.perform(post("/users").content(json).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(userExpected.getId())))
                .andExpect(jsonPath("name", is(userExpected.getName())))
                .andExpect(jsonPath("email", is(userExpected.getEmail())))
                .andExpect(jsonPath("password", is(userExpected.getPassword())))
                .andExpect(jsonPath("created", is(format(userExpected.getCreated()))))
                .andExpect(jsonPath("modified", is(format(userExpected.getModified()))))
                .andExpect(jsonPath("lastLogin", is(format(userExpected.getLastLogin()))))
                .andExpect(jsonPath("phones[0].ddd", is(phones.get(0).getDdd())))
                .andExpect(jsonPath("phones[0].number", is(phones.get(0).getNumber())));

        verify(userService, times(1)).save(user);

        verifyNoMoreInteractions(userService);
    }

    @Test
    public void givenVerbPutUsers_ThenShouldUpdateUserWithNewData_AndReturnTheNewUserData() throws Exception {
        User user = new User();
        user.setId("14416af3a404970d0b9ff2a4d5f9c2");
        user.setName("Manoel Saraiva");
        user.setEmail("massa@test.com");
        user.setPassword("password");

        User userExpected = new User();
        userExpected.setId("14416af3a404970d0b9ff2a4d5f9c2");
        userExpected.setName("Manoel Silva Saraiva");
        userExpected.setEmail("massa_updated@test.com");
        userExpected.setPassword("password2");

        when(userService.getById("14416af3a404970d0b9ff2a4d5f9c2")).thenReturn(user);
        when(userService.update(user)).then(invocation -> invocation.getArguments()[0]);

        String json = mapper.writeValueAsString(userExpected);
        mockMvc.perform(put("/users/14416af3a404970d0b9ff2a4d5f9c2").content(json).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(userExpected.getId())))
                .andExpect(jsonPath("name", is(userExpected.getName())))
                .andExpect(jsonPath("email", is(userExpected.getEmail())))
                .andExpect(jsonPath("password", is(userExpected.getPassword())))
                .andExpect(jsonPath("created", is(format(userExpected.getCreated()))))
                .andExpect(jsonPath("modified", is(format(userExpected.getModified()))))
                .andExpect(jsonPath("lastLogin", is(format(userExpected.getLastLogin()))));

        verify(userService, times(1)).update(user);
        verify(userService, times(1)).getById("14416af3a404970d0b9ff2a4d5f9c2");

        verifyNoMoreInteractions(userService);
    }

    @Test
    public void givenVerbPostUserPhone_AndUserHavePhonesEmpty_ThenShouldUpdateUserPhonesWithNewData_AndReturnTheNewUserData() throws Exception {
        User user = getModel();
        user.setId("14416af3a404970d0b9ff2a4d5f9c2");
        user.setPhones(new ArrayList<>());

        User userExpected = new User();
        userExpected.setId("14416af3a404970d0b9ff2a4d5f9c2");
        userExpected.setName("Manoel Saraiva");
        userExpected.setEmail("massa@test.com");
        userExpected.setPassword("password");

        Phone phone = new Phone(81, 4983_0912);
        List<Phone> phones = new ArrayList<>();
        phones.add(phone);
        userExpected.setPhones(phones);

        when(userService.getById("14416af3a404970d0b9ff2a4d5f9c2")).thenReturn(user);
        when(userService.update(isA(User.class))).then(invocation -> invocation.getArguments()[0]);

        String json = mapper.writeValueAsString(phone);
        mockMvc.perform(post("/users/14416af3a404970d0b9ff2a4d5f9c2/phones").content(json).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(userExpected.getId())))
                .andExpect(jsonPath("name", is(userExpected.getName())))
                .andExpect(jsonPath("email", is(userExpected.getEmail())))
                .andExpect(jsonPath("password", is(userExpected.getPassword())))
                .andExpect(jsonPath("created", is(format(userExpected.getCreated()))))
                .andExpect(jsonPath("modified", is(format(userExpected.getModified()))))
                .andExpect(jsonPath("lastLogin", is(format(userExpected.getLastLogin()))))
                .andExpect(jsonPath("phones[0].ddd", is(phone.getDdd())))
                .andExpect(jsonPath("phones[0].number", is(phone.getNumber())));

        verify(userService, times(1)).getById("14416af3a404970d0b9ff2a4d5f9c2");
        verify(userService, times(1)).update(isA(User.class));

        verifyNoMoreInteractions(userService);
    }

    @Test
    public void givenVerbPutUsersPhone_AndChangeNumberPhone_ThenShouldUpdateUserPhonesWithNewData_AndReturnTheNewUserData() throws Exception {
        List<Phone> phones = new ArrayList<>();
        phones.add(new Phone(81, 4983_0912));

        User user = getModel();
        user.setId("14416af3a404970d0b9ff2a4d5f9c2");
        user.setPhones(phones);

        User userExpected = getModel();
        userExpected.setId("14416af3a404970d0b9ff2a4d5f9c2");

        List<Phone> phonesExpected = new ArrayList<>();
        Phone phone = new Phone(81, 2223_0912);
        phonesExpected.add(phone);
        userExpected.setPhones(phonesExpected);

        when(userService.getById("14416af3a404970d0b9ff2a4d5f9c2")).thenReturn(user);
        when(userService.update(user, "14416af3a404970d0b9ff2a4d5f9c2", phone)).thenReturn(userExpected);

        String json = mapper.writeValueAsString(phone);
        mockMvc.perform(put("/users/14416af3a404970d0b9ff2a4d5f9c2/phones/14416af3a404970d0b9ff2a4d5f9c2").content(json).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is(userExpected.getId())))
                .andExpect(jsonPath("name", is(userExpected.getName())))
                .andExpect(jsonPath("email", is(userExpected.getEmail())))
                .andExpect(jsonPath("password", is(userExpected.getPassword())))
                .andExpect(jsonPath("created", is(format(userExpected.getCreated()))))
                .andExpect(jsonPath("modified", is(format(userExpected.getModified()))))
                .andExpect(jsonPath("lastLogin", is(format(userExpected.getLastLogin()))))
                .andExpect(jsonPath("phones[0].ddd", is(phone.getDdd())))
                .andExpect(jsonPath("phones[0].number", is(phone.getNumber())));

        verify(userService, times(1)).update(user, "14416af3a404970d0b9ff2a4d5f9c2", phone);
        verify(userService, times(1)).getById("14416af3a404970d0b9ff2a4d5f9c2");

        verifyNoMoreInteractions(userService);
    }

    @Test
    public void givenVerbDeleteUsers_AndTheId_ThenShouldRemoveIt() throws Exception {
        mockMvc.perform(delete("/users/14416af3a404970d0b9ff2a4d5f9c2").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
        verify(userService, times(1)).remove("14416af3a404970d0b9ff2a4d5f9c2");

        verifyNoMoreInteractions(userService);
    }

    @Test
    public void givenVerbDeleteUsersPhone_AndTheId_ThenShouldRemoveIt() throws Exception {
        mockMvc.perform(delete("/users/14416af3a404970d0b9ff2a4d5f9c2/phones/14416af3a404970d0b9ff2a4d5f9c2").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
        verify(userService, times(1)).removePhone("14416af3a404970d0b9ff2a4d5f9c2", "14416af3a404970d0b9ff2a4d5f9c2");

        verifyNoMoreInteractions(userService);
    }
}