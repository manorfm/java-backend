package br.com.user.rest.domain.user.validate;

import br.com.user.rest.domain.user.exception.InvalidUserCredentialException;
import br.com.user.rest.domain.user.exception.UserCredentialNullException;
import br.com.user.rest.domain.user.exception.UserResourceEmptyException;
import br.com.user.rest.domain.user.resources.UserResource;
import br.com.user.rest.domain.user.validade.UserValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class UserValidateTest {

    @Test
    public void givenNullUser_ThenShouldThrowUserResourceEmptyException_AndValidateErrorMessage() {
        UserResource user = null;
        Exception exception = Assertions.assertThrows(UserResourceEmptyException.class, () -> {
            UserValidator.validate(user);
        });

        assertThat(exception.getMessage(), equalTo("User data have to be informed"));
    }

    @Test
    public void givenEmptyUser_ThenShouldThrowUserResourceEmptyException_AndValidateErrorMessage() {
        UserResource user = new UserResource();
        Exception exception = Assertions.assertThrows(UserResourceEmptyException.class, () -> {
            UserValidator.validate(user);
        });

        assertThat(exception.getMessage(), equalTo("User data have to be informed"));
    }

    @Test
    public void givenEmptyPassword_ThenShouldThrowUserCredentialNullException_AndValidateErrorMessage() {
        UserResource user = new UserResource();
        user.setName("Manoel");
        Exception exception = Assertions.assertThrows(UserCredentialNullException.class, () -> {
            UserValidator.validate(user);
        });

        assertThat(exception.getMessage(), equalTo("User email and password need to be informed"));
    }

    @Test
    public void givenUserEmail_AndEmptyPassword_ThenShouldThrowUserCredentialNullException_AndValidateErrorMessage() {
        UserResource user = new UserResource();
        user.setName("Manoel");
        user.setEmail("teste@teste.com");

        Exception exception = Assertions.assertThrows(UserCredentialNullException.class, () -> {
            UserValidator.validate(user);
        });

        assertThat(exception.getMessage(), equalTo("User email and password need to be informed"));
    }

    @Test
    public void givenUserPassword_AndEmptyEmail_ThenShouldThrowUserCredentialNullException_AndValidateErrorMessage() {
        UserResource user = new UserResource();
        user.setName("Manoel");
        user.setPassword("a01ojf9om");

        Exception exception = Assertions.assertThrows(UserCredentialNullException.class, () -> {
            UserValidator.validate(user);
        });

        assertThat(exception.getMessage(), equalTo("User email and password need to be informed"));
    }

    @Test
    public void givenUserInvalidEmail_ThenShouldThrowInvalidUserCredentialException_AndValidateErrorMessage() {
        UserResource user = new UserResource();
        user.setName("Manoel");
        user.setPassword("a01ojf9om");
        user.setEmail("teste");

        Exception exception = Assertions.assertThrows(InvalidUserCredentialException.class, () -> {
            UserValidator.validate(user);
        });

        assertThat(exception.getMessage(), equalTo("Email is not valid"));
    }
}