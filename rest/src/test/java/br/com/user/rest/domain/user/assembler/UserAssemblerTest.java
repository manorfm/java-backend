package br.com.user.rest.domain.user.assembler;

import br.com.user.domain.user.User;
import br.com.user.rest.domain.user.resources.UserResource;
import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class UserAssemblerTest {

    @Test
    public void givenAUser_ThenShouldReturnOneInstanceOfUserResource_AndTheAttributesAreEquals() {
        User user = new User();
        user.setName("Manoel Sabino");
        user.setEmail("massa@hotmail.com");
        user.setPassword("1234");

        UserResourcesAssembler userResourceAssembler = new UserResourcesAssembler();
        UserResource resource = userResourceAssembler.convert(user);

        assertThat(user.getName(), equalTo(resource.getName()));
        assertThat(user.getEmail(), equalTo(resource.getEmail()));
        assertThat(user.getPassword(), equalTo(resource.getPassword()));
    }
}
