# Java backend

java backend project constructed with java8, gradle, spring boot, spring security, spring data, junit, lombok, orika, jwt, mariadb (on heroku) and hsqldb (run tests)
and deployed on heroku on dyno: http://damp-garden-05339


### APIs
we have an user on heroku db and we can starting doing login with this user:
<br/>
email: teste@teste.com
password: 1234

so we can send a post to 
http://damp-garden-05339.herokuapp.com/login

and should send on the body of the request something like:

```` javascriot
    {
        "email": "manoel@concrete.com",
        "password": "1234"
    }
````

so, after that will return a token and a less that the email or password are not right

with the token in hands we can use it to have access to the others entry points 
and they are
 
 #### We have links to manager users
 we can use a get verb on http://damp-garden-05339.herokuapp.com/users to get all users saved
 
 we can use a get verb on http://damp-garden-05339.herokuapp.com/users/{id} to get a user by id, the id as UUID
 
 we can use a post verb on http://damp-garden-05339.herokuapp.com/users to save a new user
 
 we can use a put verb on http://damp-garden-05339.herokuapp.com/users to update a new user
 
 we can use a delete verb on http://damp-garden-05339.herokuapp.com/users to delete a new user
 
 
 #### We have also some links to phones
 we can use a get verb on http://damp-garden-05339.herokuapp.com/users/{id}/phones to return all phone of that user
 
 we can use a get verb on http://damp-garden-05339.herokuapp.com/users/{id}/phones/{id} to get one phone of the user
 
we can use a post verb on http://damp-garden-05339.herokuapp.com/users/{id}/phones to save a new phone

we can use a put verb on http://damp-garden-05339.herokuapp.com/users/{id}/phones/{id} to update a new phone of the user

we can use a delete verb on http://damp-garden-05339.herokuapp.com/users/{id}/phones/{id} to delete a phone of the user


###
the project can be build with gradle
you can use

```command
    gradle build
```

to run all test
```command
    gradle test
```

to run run the application locally pointing to database on heroku
```command
    gradle bootRun -q -DPORT=8080
```
the default port is 5000 and you can change setting PORT={port} as environment variable



