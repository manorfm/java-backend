package br.com.user.security;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

@Component
public class KeyEncoder implements PasswordEncoder {

    @Override
    public String encode(CharSequence rawPassword) {
        String password = rawPassword.toString();
        try {
            return KeyUtil.doHash(password, KeySecurity.KEY);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return password;
    }

    public static void main(String[] args) {
        System.out.println(new KeyEncoder().encode("1234"));
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return encodedPassword.equals(encode(rawPassword));
    }
}
