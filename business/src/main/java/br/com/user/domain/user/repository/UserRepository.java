package br.com.user.domain.user.repository;

import java.util.List;
import java.util.Optional;

import br.com.user.domain.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    
	List<User> findAll();
	
	User findOneByEmail(String email);

	Optional<User> findById(String id);
}