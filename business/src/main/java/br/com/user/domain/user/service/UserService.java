package br.com.user.domain.user.service;

import br.com.user.domain.user.Phone;
import br.com.user.domain.user.User;
import br.com.user.domain.user.repository.UserRepository;
import br.com.user.exception.UserAlreadyExistException;
import br.com.user.exception.UserNotFoundException;
import br.com.user.security.KeySecurity;
import br.com.user.security.KeyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Transactional
	public User save(User user) {
		try {
			if (!exist(user)) {
				user.setPassword(KeyUtil.doHash(user.getPassword(), KeySecurity.KEY));
				return userRepository.save(user);
			} else {
				throw new UserAlreadyExistException(user.getName());
			}
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	private boolean exist(User user) {
		return user.getId() != null || userRepository.findOneByEmail(user.getEmail()) != null;
	}

	@Transactional
	public User update(User user) {
		return userRepository.save(user);
	}

	@Transactional
	public User update(User user, String phoneId, Phone phone) {
		Optional<Phone> result = user.getPhones().stream().filter(p -> p.getId().equals(phoneId)).findAny();

		if (result.isPresent()) {
			Phone userPhone = result.get();
			userPhone.setDdd(phone.getDdd());
			userPhone.setNumber(phone.getNumber());
		}

		return userRepository.save(user);
	}
	
	private User findOne(String id) {
		if (id == null) {
			return null;
		}

		Optional<User> user = userRepository.findById(id);
		if (user.isPresent()) {
			return user.get();
		}
		return null;
	}

	@Transactional
    public void remove(User user) {
	    userRepository.delete(user);
    }

	@Transactional
    public void remove(String id) {
		User user = getById(id);
	    userRepository.delete(user);
    }

    @Transactional
    public void removePhone(String id, String idPhone) {
		User user = getById(id);
		user.getPhones().removeIf(phone -> phone.getId().equals(idPhone));
	    update(user);
    }
	
	@Transactional
	public User getById(String id) {
		User user = findOne(id);
		return validate(user);
	}

	@Transactional
	public User get(String email) {
		User user = userRepository.findOneByEmail(email);
		return validate(user);
	}

	private User validate(User user) {
		if (user == null) {
			throw new UserNotFoundException("Usu�rio e/ou senha inv�lidos");
		}
		return user;
	}

	@Transactional
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Transactional
	public User registerLogin(String email) {
		User user = userRepository.findOneByEmail(email);
		user.setLastLogin(LocalDateTime.now());

		return update(user);
	}
}