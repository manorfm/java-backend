package br.com.user.domain.user;

import br.com.user.domain.PersistenceObject;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "phone")
@EqualsAndHashCode(callSuper=false)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Phone extends PersistenceObject {

    @Column(name = "phn_ddd")
    private int ddd;

    @Column(name = "phn_num")
    private int number;
}
