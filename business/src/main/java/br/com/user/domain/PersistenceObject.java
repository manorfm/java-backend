package br.com.user.domain;

import lombok.EqualsAndHashCode;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import java.util.UUID;

@MappedSuperclass
@EqualsAndHashCode(callSuper = false)
@Data
@ToString
public abstract class PersistenceObject extends AuditObject {

	private static final long serialVersionUID = -2387520509257475867L;

	/*@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;*/

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	public String id;
	
}