package br.com.user.domain.user;

import br.com.user.domain.PersistenceObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user")
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
@Data
public class User extends PersistenceObject {

    private static final long serialVersionUID = 4439931903656975507L;

    @Column(name = "usr_email")
    private String email;

    @Column(name = "usr_nm")
    private String name;

    @Column(name = "usr_pw")
    private String password;

    @Column(name="usr_lst_login")
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime lastLogin;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    private List<Phone> phones;

    public void add(Phone phone) {
        if (phones == null) {
            phones = new ArrayList<>();
        }
        phones.add(phone);
    }
}
