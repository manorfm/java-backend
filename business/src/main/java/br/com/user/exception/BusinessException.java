package br.com.user.exception;

import lombok.Getter;

public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 3933963804152709226L;

	@Getter
	private ErrorMessage errorMessage;
	
	public BusinessException(ErrorMessage message) {
		super(message.getMessage());
		this.errorMessage = message;
	} 

	public BusinessException(ErrorMessage message, Throwable e) {
		super(message.getMessage(), e);
		this.errorMessage = message;
	}
}
