package br.com.user.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ErrorMessage {

	private String error;
	private String message;
	
}
