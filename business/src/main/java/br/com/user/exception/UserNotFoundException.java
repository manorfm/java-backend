package br.com.user.exception;

public class UserNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 5186765795932546148L;

	public UserNotFoundException() { }

	public UserNotFoundException(String message) {
		super(message);
	}
}
