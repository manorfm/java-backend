package br.com.user.exception;

public class UserAlreadyExistException extends RuntimeException {

	private static final long serialVersionUID = 5186765795932546148L;

	public UserAlreadyExistException(String email) {
		super(String.format("User with email: %s already registred.", email));
	}
}
