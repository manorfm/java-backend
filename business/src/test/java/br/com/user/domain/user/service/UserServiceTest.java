package br.com.user.domain.user.service;

import br.com.user.domain.config.ApplicationConfigTest;
import br.com.user.domain.user.Phone;
import br.com.user.domain.user.User;
import br.com.user.security.KeyEncoder;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ApplicationConfigTest.class)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private KeyEncoder keyEncoder;

    private String userId;

    @Before
    public void startupObject() {
        List<User> users = userService.findAll();
        users.stream().forEach(user -> userService.remove(user.getId()));

        User user = new User();
        user.setName("Manoel Medeiros");
        user.setEmail("manoel@teste.com");
        user.setPassword("1234");

        User userSaved = userService.save(user);

        this.userId = userSaved.getId();
    }

    @Test
    public void givenSavedUser_ThenValidateIfThePasswordAreEncrypted_AndTheOthersAttributes() {
        User user = userService.getById(this.userId);

        String encrypted = keyEncoder.encode("1234");

        assertThat(user, notNullValue());
        MatcherAssert.assertThat(user.getId(), notNullValue());
        assertThat(user.getName(), equalTo("Manoel Medeiros"));
        assertThat(user.getEmail(), equalTo("manoel@teste.com"));
        assertThat(user.getPassword(), equalTo(encrypted));
    }

    @Test
    public void givenUser_AndUpdatedValues_ThenShouldUpdateData_AndReturnUserWithNewData() {
        User user = userService.getById(this.userId);

        user.setName("Manoel updated");
        user.setEmail("manoel@updated.com");

        User userUpdated = userService.update(user);

        assertThat(userUpdated.getName(), equalTo(user.getName()));
        assertThat(userUpdated.getEmail(), equalTo(user.getEmail()));
    }

    @Test
    public void givenUser_AndRemoveIt_ThenTheDataBaseShouldBeEmpty() {
        User user = userService.getById(this.userId);

        userService.remove(user.getId());

        List<User> users = userService.findAll();

        assertThat(users, emptyIterable());
    }

    @Test
    public void givenUserWithEmptyPhones_ThenAddPhone_AndTheUpdateResultShouldReturnUserWithDataUpdated() {
        User user = userService.getById(this.userId);

        Phone phone = new Phone();
        phone.setNumber(3456_6745);
        phone.setDdd(81);

        user.add(phone);
        User userUpdated = userService.update(user);

        assertThat(userUpdated, notNullValue());
        MatcherAssert.assertThat(userUpdated.getId(), notNullValue());
        assertThat(userUpdated.getName(), equalTo("Manoel Medeiros"));
        assertThat(userUpdated.getEmail(), equalTo("manoel@teste.com"));
        assertThat(userUpdated.getPhones().get(0).getDdd(), equalTo(phone.getDdd()));
        assertThat(userUpdated.getPhones().get(0).getNumber(), equalTo(phone.getNumber()));
    }

    @Test
    public void givenUserWithEmptyPhones_AndAddPhone_ThenUserPhoneShouldHaveBeenAdded() {
        User user = userService.getById(this.userId);

        Phone phone = new Phone();
        phone.setNumber(3456_6745);
        phone.setDdd(81);

        user.add(phone);
        userService.update(user);
        User userUpdated = userService.getById(user.getId());

        assertThat(userUpdated, notNullValue());
        MatcherAssert.assertThat(userUpdated.getId(), notNullValue());
        assertThat(userUpdated.getName(), equalTo("Manoel Medeiros"));
        assertThat(userUpdated.getEmail(), equalTo("manoel@teste.com"));
        assertThat(userUpdated.getPhones().get(0).getDdd(), equalTo(phone.getDdd()));
        assertThat(userUpdated.getPhones().get(0).getNumber(), equalTo(phone.getNumber()));
    }

    @Test
    public void givenUserWithPhones_AndAddPhone_ThenUserPhoneShouldHaveBeenAdded() {
        User user = userService.getById(this.userId);

        Phone phone = new Phone();
        phone.setNumber(3456_6745);
        phone.setDdd(81);

        user.add(phone);
        User userUpdated = userService.update(user);

        userService.removePhone(this.userId, userUpdated.getPhones().get(0).getId());

        User userExpected = userService.getById(this.userId);

        assertThat(userExpected, notNullValue());
        MatcherAssert.assertThat(userExpected.getId(), notNullValue());
        assertThat(userExpected.getName(), equalTo("Manoel Medeiros"));
        assertThat(userExpected.getEmail(), equalTo("manoel@teste.com"));
        assertThat(userExpected.getPhones(), emptyIterable());
    }
}
