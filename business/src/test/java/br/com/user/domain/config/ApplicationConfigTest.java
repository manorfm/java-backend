package br.com.user.domain.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(basePackages = {
		"br.com.user.converter",
		"br.com.user.domain",
		"br.com.user.security",
		"br.com.user.service"})

@EnableJpaRepositories(basePackages = {"br.com.user.domain"})
@EntityScan("br.com.user.domain")
@ConfigurationProperties
@SpringBootApplication
public class ApplicationConfigTest {

}
/*

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "br.com.user.domain" })
@ComponentScan(basePackages = {
		"br.com.user.rest",
		"br.com.user.domain",
		"br.com.user.service",
		"br.com.user.security"
})
@EntityScan("br.com.user.domain")
@EnableAspectJAutoProxy
@SpringBootConfiguration
@EnableAutoConfiguration*/
