package br.com.user.main.security.jwt.config.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException {
        final String expired = String.class.cast(request.getAttribute("expired"));

        String message = "N�o autorizado";
        if (expired != null) {
            message = "Sess�o inv�lida";
        }

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType("application/json");

        String errorMessage = String.format("{ \"error\": \"%s\"}", message);
        response.getWriter().write(new String(errorMessage.getBytes(), "UTF-8"));
    }
}
