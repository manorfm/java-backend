package br.com.user.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(basePackages = {
        "br.com.user.converter",
        "br.com.user.domain",
        "br.com.user.security",
        "br.com.user.rest",
        "br.com.user.service",
        "br.com.user.main.security.jwt",
        "br.com.user.type.converters"})

@EnableJpaRepositories(basePackages = {"br.com.user.domain"})
@EntityScan("br.com.user.domain")
@ConfigurationProperties
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}