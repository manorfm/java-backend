for /f %%i in ('heroku config:get JAWSDB_URL -a damp-garden-05339') do set VAR=%%i

 java -jar .\main\build\libs\main-1.0.0-boot.jar --spring.datasource.url=%VAR% ^
     --spring.datasource.driver-class-name=com.mysql.jdbc.Driver ^
     --spring.datasource.username=%USER_NAME% ^
     --spring.datasource.password=%USER_PASSWORD% ^
     --spring.datasource.hibernate.ddl-auto=create ^
     --server.port=8080