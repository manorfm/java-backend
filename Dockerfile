FROM java
MAINTAINER Manoel Rodrigo Farinha de Medeiros <manorfm@hotmail.com>

ENV DIR_BASE=/usr/app

RUN mkdir -p $DIR_BASE

ADD ./main/target/main-1.0.0.war $DIR_BASE/manager.war

WORKDIR $DIR_BASE

CMD ["java", "-jar", "manager.war"]